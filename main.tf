/*
 * Read Terraform document for more configuration for ECS service resource at here:
 * - https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_service
*/
resource "aws_ecs_service" "default" {
  cluster            = var.aws_ecs_cluster_id
  launch_type        = "EC2"
  
  name               = var.service.name
  task_definition    = var.service.task_definition_arn
  desired_count      = var.service.desired_count
  iam_role           = var.ecs_iam_role_arn

  load_balancer {
    target_group_arn = aws_alb_target_group.service-target-group.arn
    container_name   = var.service.container_name
    container_port   = var.service.container_port
  }
}


/*
 * Create target group for each ECS service
*/
resource "aws_alb_target_group" "service-target-group" {
  vpc_id                = var.vpc_id

  name                  = var.service.name
  port                  = var.service.host_port
  protocol              = var.service.protocol

  health_check {
    path                = var.service.health_check_path
    port                = "traffic-port"
    healthy_threshold   = 5
    unhealthy_threshold = 2
    timeout             = 2
    interval            = 5
    matcher             = "200"
  }
}

/*
 * Create listener rule for HTTP traffic.
*/
resource "aws_alb_listener" "http" {
  load_balancer_arn     = var.load_balancer_arn
  port                  = "80"
  protocol              = "HTTP"
  
  default_action {
    type                = "forward"
    target_group_arn    = var.default_target_group_arn == ""? aws_alb_target_group.service-target-group.arn : var.default_target_group_arn
  }
}



resource "aws_alb_listener_rule" "http" {
  count              = var.create_aws_alb_listener_rule ? 1 : 0

  listener_arn       = aws_alb_listener.http.arn
  priority           = 100

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.service-target-group.arn
  }

  condition {
    path_pattern {
      values         = [var.aws_alb_listener_rule_path]
    }
  }

  depends_on = [aws_alb_listener.http]
}

/*
* Create listener rule for HTTPS traffic.
*/
resource "aws_alb_listener" "https" {
  load_balancer_arn  = var.load_balancer_arn
  port               = "443"
  protocol           = "HTTPS"
  ssl_policy         = "ELBSecurityPolicy-2016-08"
  certificate_arn    = var.acm_certificate_arn

  default_action {
    type             = "forward"
    target_group_arn = var.default_target_group_arn == ""? aws_alb_target_group.service-target-group.arn : var.default_target_group_arn
  }
}


/*
 * Add listener rule to forward HTTPS traffic at path /backend/* to ECS backend service (Django)
*/
resource "aws_alb_listener_rule" "https" {
  count              = var.create_aws_alb_listener_rule ? 1: 0
  
  listener_arn       = aws_alb_listener.https.arn
  priority           = 100

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.service-target-group.arn
  }

  condition {
    path_pattern {
      values = [var.aws_alb_listener_rule_path]
    }
  }
  depends_on = [aws_alb_listener.https]
}
