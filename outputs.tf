output "ecs_service_arn" {
  value = aws_ecs_service.default.cluster
}

output "ecs_service_name" {
  value = aws_ecs_service.default.name
}

output "aws_alb_target_group_arn" {
  value = aws_alb_target_group.service-target-group.arn
}

output "aws_alb_target_group_arn_suffix" {
  value = aws_alb_target_group.service-target-group.arn_suffix
}

output "aws_alb_target_group_name" {
  value = aws_alb_target_group.service-target-group.name
}